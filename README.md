# 1c Server and PostgreSQL-1c.
I didn't use docker-compose intentionally, so this two folders will contain build scripts to compile 1C server and patched postgresql for it.

## Downloading
You should download a bundle for Ubuntu\Debian for 1C SQL Platform and PostgreSQL server using [This link](https://releases.1c.ru/total).

## Installing 1C
* Extract all files of downloaded bundle to docker-1c-server/container/1c/
* Change ENV according your 1c version in docker-1c-server/Dockerfile
	EX:
ENV PLATFORM_VERSION 83
ENV SERVER_VERSION 8.3.13-1644
* Examine docker-1c-server/run and make sure mount directories exist and have 777 permissions. Or create a user with uid=999 and gid=999 and make this user and owner of those directories. 
* Also make sure you provide hostname as 1c server is making workers base on this name. If you encounter error while creating or connecting to database like: Cann't find server "hostname", you need to either add DNS record to point to this server or add a hosts files record.
* Now we need to build it. It is as simple as:
 # cd docker-1c-server
 # chmod u+x build.sh
 # ./build.sh
Should be good to go with starting.
* Starting:
 # cd docker-1c-server
 # chmod u+x ./run.sh
 # ./run.sh

##Installing PostgreSQL
* PosgreSQL bundle have 3 folders inside the archive. Uncompress the archive somewhere and execute:
 # find ./ -name "*.deb" -exec cp {} pathTO/docker-postgrespro-1c/container/postgres/ \;
Or simply: move all .deb files to this location without folders
* Check the postgres configuration and modify the file according your hardware and your needs: docker-postgrespro-1c/container/postgresql.conf.sh
* If you are installing PostgreSQL of different version then 10, change version number in: docker-postgrespro-1c/Dockerfile
	EX:
ENV SERVER_VERSION 10
* Building is simple:
 # cd docker-postgrespro-1c
 # chmod u+x ./build.sh
 # ./build.sh
* Now, before starting, make sure you provided postgres user's password and mount directory exists from docker-postgrespro-1c/run.sh
* Start:
 # cd docker-postgrespro-1c
 # chmod u+x ./run.sh
 # ./run.sh

We are done!
