#!/bin/bash

cat << EOF > $PGDATA/postgresql.conf

bgwriter_delay = 80ms
bgwriter_lru_multiplier = 8.0
bgwriter_lru_maxpages = 3000
listen_addresses = '*'
port = 5432				
max_connections = 3000
shared_buffers = 1200MB
huge_pages = off
temp_buffers = 400MB
max_prepared_transactions =3 
work_mem = 800MB				
maintenance_work_mem = 500MB		
autovacuum_work_mem = 500MB		
#dynamic_shared_memory_type = posix	
max_files_per_process = 3000		
effective_io_concurrency = 8
max_worker_processes = 8
max_parallel_workers = 4
fsync = off
synchronous_commit = off
wal_sync_method = fsync		
#wal_sync_method = fdatasync
archive_mode = off
enable_bitmapscan = on
enable_hashagg = on
enable_hashjoin = on
enable_indexscan = on
enable_indexonlyscan = off
enable_material = on
enable_mergejoin = on
enable_nestloop = on
enable_seqscan = on
enable_sort = on
enable_tidscan = on
seq_page_cost = 0.01
random_page_cost = 0.01	
cpu_tuple_cost = 0.01
cpu_index_tuple_cost = 5.01
cpu_operator_cost = 0.01
#parallel_tuple_cost = 0.1		
#parallel_setup_cost = 0.1
min_parallel_table_scan_size = 100MB
min_parallel_index_scan_size = 100MB
effective_cache_size = 500MB
geqo = on
log_line_prefix = '%m [%p] %q%u@%d '		
autovacuum = on			
autovacuum_max_workers = 3
datestyle = 'iso, mdy'
standard_conforming_strings = off
max_parallel_workers_per_gather = 2
standard_conforming_strings = off
#escape_string_warning = off
#max_locks_per_transaction = 512
#max_wal_size = 521MB
#wal_level = minimal
max_wal_senders = 0
EOF
