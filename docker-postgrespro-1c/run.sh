#!/bin/sh

docker run --name postgrespro-1c \
  --restart unless-stopped \
  --net host \
  --detach \
  --mount type=bind,source=/opt/postgrespro-1c-data/_data,target=/data \
  --volume /etc/localtime:/etc/localtime:ro \
  --env POSTGRES_PASSWORD=123 \
  lethal/postgrespro-1c
