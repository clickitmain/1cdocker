#!/bin/sh

docker run --name 1c-server \
  --restart unless-stopped \
  --net host \
  --detach \
  --mount type=bind,source=/opt/1c-server-home/_data,target=/home/usr1cv8 \
  --mount type=bind,source=/opt/1c-server-logs/_data,target=/var/log/1C \
  --volume /etc/localtime:/etc/localtime:ro \
  lethal/1c-server
